﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CodeInterpreter
{
    public class SourceCodeLoader
    {
        private static string CODEDIRECTORY = "./Examples/";

        public SourceCodeLineList loadCodeFile(string fileName)
        {
            List<string> returnList = new List<string>();
            SourceCodeError codeError = null;
            if (File.Exists(CODEDIRECTORY+ fileName))
            {
                try
                {
                    returnList = File.ReadAllLines(CODEDIRECTORY + fileName).ToList();
                }
                catch (Exception e)
                {
                    codeError = new SourceCodeError("Code file loading failed", e);
                }
            }
            else
            {
                codeError = new SourceCodeError("Code file does not exist in \"" + CODEDIRECTORY + "\"");
            }
            SourceCodeLineList codeList = loadCode(returnList);
            if (codeError != null)
            {
                codeList.SourceCodeErrors.Add(codeError);
            }
            return codeList;
        }

        public SourceCodeLineList loadCode(List<string> codeText)
        {
            int counter = 1;
            SourceCodeLineList codeList = new SourceCodeLineList();
            foreach (string codeLine in codeText)
            {
                codeList.Add(new SourceCodeLine(counter,codeLine));
                counter += 1;
            }
            return codeList;
        }
    }
}
