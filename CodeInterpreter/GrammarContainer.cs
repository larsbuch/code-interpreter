﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CodeInterpreter
{
    public class GrammarContainer
    {
        private Dictionary<string, Grammar> _grammarContainer;

        public GrammarContainer()
        {
            _grammarContainer = new Dictionary<string, Grammar>();
        }

        public void AddGrammar(Grammar grammar)
        {
            _grammarContainer.Add(grammar.GrammarName, grammar);
        }

        public Grammar GetGrammar(string grammarName)
        {
            Grammar grammar;
            if(_grammarContainer.TryGetValue(grammarName, out grammar))
            {
                return grammar;
            }
            else
            {
                throw new Exception(string.Format("Grammar {0} does not exist",grammarName));
            }
        }
    }
}
