﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CodeInterpreter
{
    /**
     * Delta Parsersplit on shift/reduce ambiguity and when ambiguity is resolved or when parser does the same they are merged
     * Symbol table entry has parser prefix on parsersplit to avoid copy on ambiguity
     * Parse tree allows a multiple parse paths when ambiguity is found and merging when ambiguity is resolved or parser is merged
     * Does a parser split when lexer has multible matching
     * . If a linked list 
     */
    public class DeltaParser
    {
        private Grammar _grammar;
        private Lexer _stepLexer;
        private List<Production> _activeProductions;
        private Stack<Production> _productionStack;

        public DeltaParser(GrammarInterpreter grammarInterpreter)
        {
            _activeProductions = new List<Production>();
            _productionStack = new Stack<Production>();
            _grammar = grammarInterpreter.GetGrammar();
           // _stepLexer = new Lexer(_grammar.GetTerminals());
        }

        //public void Parse()
        //{
        //    // TODO setup
        //    bool finished = false;
        //    while(finished)
        //    {
        //        finished = !ParseToken();
        //    }
        //}

        //private bool ParseToken()
        //{
        //    if(_stepLexer.CurrentToken.Terminal != "EOF")
        //    {
        //        Token token = _stepLexer.NextToken();
        //        foreach (Production production in _activeProductions)
        //        {
        //            if (production.MatchToken(token))
        //            {
        //                // TODO
        //            }
        //        }


        //        return false;
        //    }
        //    else
        //    {
        //        return true;
        //    }
        //}
    }
}
