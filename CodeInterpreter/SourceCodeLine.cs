﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CodeInterpreter
{
    public class SourceCodeLine
    {
        private string _line;
        private int _lineNumber;

        public SourceCodeLine(int lineNumber, string line)
        {
            _lineNumber = lineNumber;
            _line = line;
        }

        public int LineNumber
        {
            get
            {
                return _lineNumber;
            }
            set
            {
                _lineNumber = value;
            }
        }

        public int Length
        {
            get
            {
                return _line.Length;
            }
        }

        public string writeCodeLine()
        {
            return _line;
        }

        public override string ToString()
        {
            return _line;
        }

        public string Substring(int startIndex, int length)
        {
            if (startIndex + length <= _line.Length)
            {
                return _line.Substring(startIndex, length);
            }
            else
            {
                throw new Exception("Trying to read after line end.");
            }
        }

        public string Substring(int startIndex)
        {
            if (startIndex < _line.Length)
            {
                return _line.Substring(startIndex);
            }
            else
            {
                throw new Exception("Trying to read after line end.");
            }
        }
    }
}
