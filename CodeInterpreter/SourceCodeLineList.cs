﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CodeInterpreter
{
    public class SourceCodeLineList
    {
        private Dictionary<int,SourceCodeLine> _sourceCodeLines;

        public List<SourceCodeError> SourceCodeErrors { get; set; }

        public SourceCodeLineList()
        {
            _sourceCodeLines = new Dictionary<int,SourceCodeLine>();
            SourceCodeErrors = new List<SourceCodeError>();
        }

        public string writeCode()
        {
            StringBuilder returnString = new StringBuilder();
            foreach (SourceCodeLine sourceCodeLine in _sourceCodeLines.Values)
            {
                returnString.AppendLine(sourceCodeLine.writeCodeLine());
            }

            return returnString.ToString();
        }

        public void parseCode(GrammarInterpreter Grammar)
        {
            // TODO parse code
        }

        public void Add(SourceCodeLine sourceCodeLine)
        {
            if (_sourceCodeLines.ContainsKey(sourceCodeLine.LineNumber))
            {
                _sourceCodeLines.Remove(sourceCodeLine.LineNumber);
            }
            _sourceCodeLines.Add(sourceCodeLine.LineNumber, sourceCodeLine);
        }

        public IEnumerable<SourceCodeLine> Lines
        {
            get
            {
                foreach (SourceCodeLine sourceCodeLine in _sourceCodeLines.Values)
                {
                    yield return sourceCodeLine;
                }
            }
        }

        public int Count
        {
            get
            {
                return _sourceCodeLines.Count;
            }
        }

        public SourceCodeLine Line(int activeLineNumber)
        {
            SourceCodeLine sourceCodeLine;
            if (_sourceCodeLines.TryGetValue(activeLineNumber, out sourceCodeLine))
            {
                return sourceCodeLine;
            }
            else
            {
                throw new Exception("Failed to get SourceCodeLine");
            }
        }
    }
}
