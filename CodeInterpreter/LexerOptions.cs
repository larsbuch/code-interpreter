﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CodeInterpreter
{
    public class LexerOptions
    {
        private bool _returnIndentTokens = false;
        private int _indentSpacePerTab = 4;
        private bool _returnLexerPathTokens = false;

        public bool ReturnIndentTokens
        {
            get
            {
                return _returnIndentTokens;
            }
            set
            {
                _returnIndentTokens = value;
            }
        }

        public int IndentSpacePerTab
        {
            get
            {
                return _indentSpacePerTab;
            }
            set
            {
                _indentSpacePerTab = value;
            }
        }

        public bool ReturnLexerPathTokens
        {
            get
            {
                return _returnLexerPathTokens;
            }
            set
            {
                _returnLexerPathTokens = value;
            }
        }
    }
}
