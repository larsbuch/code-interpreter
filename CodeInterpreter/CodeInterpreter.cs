﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CodeInterpreter
{
    public class CodeInterpreter
    {
        public GrammarInterpreter Grammar { get; private set; }

        public void parseGrammarFile(string fileName)
        {
            GrammarLoader grammarLoader = new GrammarLoader();
            Grammar = grammarLoader.loadGrammarFile(fileName);
            Grammar.parseGrammar();
        }

        public bool HasGrammarErrors 
        { 
            get 
            {
                return GrammarErrors.Count > 0;
            } 
        }

        public List<GrammarError> GrammarErrors 
        {
            get
            {
                return Grammar.GrammarErrors;
            }
        }

        public SourceCodeLineList Code { get; private set; }

        public void parseCodeText(List<string> codeText)
        {
            SourceCodeLoader codeLoader = new SourceCodeLoader();
            Code = codeLoader.loadCode(codeText);
            parseCode();
        }

        public void parseCodeFile(string fileName)
        {
            SourceCodeLoader codeLoader = new SourceCodeLoader();
            Code = codeLoader.loadCodeFile(fileName);
            parseCode();
        }

        private void parseCode()
        {
            if (Code != null)
            {
                Code.parseCode(Grammar);
            }
        }

        public bool HasCodeErrors
        {
            get 
            {
                return CodeErrors.Count > 0;
            } 
        }

        public List<SourceCodeError> CodeErrors
        {
            get
            {
                return Code.SourceCodeErrors;
            }
        }

        public string writeCode()
        {
            return Code.writeCode();
        }

        public string writeGrammar()
        {
            StringBuilder stringBuilder = new StringBuilder();
            Grammar.writeGrammar(stringBuilder);
            return stringBuilder.ToString();
        }
    }
}
