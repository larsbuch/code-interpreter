﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CodeInterpreter
{
    class CodeInterpreterConsole
    {
        static void Main(string[] args)
        {
            TestTokenizer();
            //TestPostalCode();

            Console.WriteLine("Press any key to exit");
            Console.ReadKey();
        }

        private static void TestTokenizer()
        {
            LexerOptions lexerOptions = new LexerOptions();
            lexerOptions.IndentSpacePerTab = 5;
            lexerOptions.ReturnIndentTokens = true;
            lexerOptions.ReturnLexerPathTokens = true;

            string testGrammarName = "TestGrammar";

            Grammar grammar = new Grammar(testGrammarName);
            grammar.AddTerminal(new Terminal("SPACE", " ", true));
            grammar.AddTerminal(new Terminal("LARS", "Lars", false));
            grammar.AddTerminal(new Terminal("RITA", "Rita", false));
            grammar.AddTerminal(new Terminal("WORD", "\\w+", false));

            Console.WriteLine("Terminals"); 
            foreach (var terminal in grammar.Terminals)
            {
                Console.WriteLine("Key: " + terminal.TerminalName + " Regex: \"" + terminal.TerminalMatch + "\" isIgnored: " + terminal.IgnoreTerminal);
            }
            Console.WriteLine("");

            GrammarContainer grammarContainer = new GrammarContainer();
            grammarContainer.AddGrammar(grammar);

            Lexer lexer = new Lexer(grammarContainer, testGrammarName, lexerOptions);

            Console.WriteLine("Tokens");
            foreach (Token token in TokenizeAll(lexer))
            {
                Console.WriteLine(token.ToString());
            }
            Console.WriteLine("");

            SourceCodeLine singleTextLine = new SourceCodeLine(1, " Lars gik en tur");
            Console.WriteLine("singleTextLine: " + singleTextLine.writeCodeLine());

            SourceCodeLineList sourceCodeLineList = new SourceCodeLineList();
            sourceCodeLineList.Add(singleTextLine);

            lexer.Reset();
            lexer.SourceCodeLines = sourceCodeLineList;

            Console.WriteLine("Tokens");
            foreach (Token token in TokenizeAll(lexer))
            {
                Console.WriteLine(token.ToString());
            }
            Console.WriteLine("");

            lexer.Reset();
            lexer.SourceCodeLines = sourceCodeLineList;

            List<string> stringList = TokenizeAll(lexer).Select(s => s.Terminal).ToList();
            Console.WriteLine("StringList of Tokens");
            foreach (string terminalString in stringList)
            {
                Console.WriteLine(terminalString);
            }
            Console.WriteLine("");

            sourceCodeLineList = new SourceCodeLineList();
            sourceCodeLineList.Add(new SourceCodeLine(1, "Lars og Rita gik en tur."));
            sourceCodeLineList.Add(new SourceCodeLine(2, "De så en isbjørn"));
            sourceCodeLineList.Add(new SourceCodeLine(3, "Senere kørte de hjem"));

            Console.WriteLine("Text lines");
            foreach (SourceCodeLine sourceCodeLine in sourceCodeLineList.Lines)
            {
                Console.WriteLine(sourceCodeLine.writeCodeLine());
            }
            Console.WriteLine("");


            lexer.Reset();
            lexer.SourceCodeLines = sourceCodeLineList;

            Console.WriteLine("Tokens");
            foreach (Token token in TokenizeAll(lexer))
            {
                Console.WriteLine(token.ToString());
            }
            Console.WriteLine("");

        }

        private static void TestPostalCode()
        {
            CodeInterpreter codeParser = new CodeInterpreter();
            codeParser.parseGrammarFile("PostalCoding");
            Console.WriteLine("Does grammar contains errors: " + codeParser.HasGrammarErrors);
            if(codeParser.HasGrammarErrors)
            {
                foreach (GrammarError grammarError in codeParser.GrammarErrors)
                {
                    Console.WriteLine("Grammar errors: " + grammarError.ToString());
                }
            }
            else
            {
                Console.WriteLine("Grammar: ");
                Console.WriteLine(codeParser.writeGrammar());

                codeParser.parseCodeFile("postal.txt");
                Console.WriteLine("Does code contains errors: " + codeParser.HasCodeErrors);
                if(codeParser.HasCodeErrors)
                {
                    foreach (SourceCodeError codeError in codeParser.CodeErrors)
                    {
                        Console.WriteLine("Code errors: " + codeError.ToString());
                    }
                }
                else
                {
                    Console.WriteLine("Code: ");
                    Console.WriteLine(codeParser.writeCode());
                }
            }
        }

        private static IEnumerable<Token> TokenizeAll(Lexer lexer)
        {
            foreach(List<Token> nextTokens in lexer.NextTokens())
            {
                foreach(Token token in nextTokens)
                {
                    yield return token;
                }
            }
        }
    }
}
