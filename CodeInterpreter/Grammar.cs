﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CodeInterpreter
{
    public class Grammar
    {
        private List<Production> _grammar;
        private List<Terminal> _terminals;
        private string _grammarName;

        public Grammar(string grammarName)
        {
            _grammarName = grammarName;
            _grammar = new List<Production>();
            _terminals = new List<Terminal>();
        }

        #region Properties

        public string GrammarName
        {
            get
            {
                return _grammarName;
            }
        }

        #endregion

        public IEnumerable<Terminal> Terminals
        {
            get
            {
                foreach(Terminal terminal in _terminals)
                {
                    yield return terminal;
                }
            }
        }

        public void AddTerminal(Terminal terminal)
        {
            _terminals.Add(terminal);
        }
    }
}
