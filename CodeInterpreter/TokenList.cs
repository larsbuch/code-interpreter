﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CodeInterpreter
{
    public class TokenList
    {
        private List<Token> _tokenList;

        public TokenList()
        {
            _tokenList = new List<Token>();
        }

        public string WriteAll()
        {
            StringBuilder stringBuilder = new StringBuilder();
            foreach (Token token in _tokenList)
            {
                stringBuilder.AppendLine(token.ToString());
            }
            return stringBuilder.ToString();
        }

        public void AddTokenFound(Token token)
        {
            _tokenList.Add(token);
        }

    }
}
