﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace CodeInterpreter
{
    /**
     * Responsible for tokenizing the input. Input is tokenized regex match done by NextToken() function. Using a stepwize 
     * lexer makes it possible to redefine terminals on the run making parsing Tex possible.
     * 
     */
    public class Lexer
    {
        #region Static
        protected static Regex _indentRegex;

        protected static Regex IndentRegex
        {
            get
            {
                if (_indentRegex == null)
                {
                    _indentRegex = new Regex("^([\t ]*)([^\t \n]+)", RegexOptions.Compiled);
                }
                return _indentRegex;
            }
        }

        #endregion


        private SourceCodeLineList _sourceCodeLineList;
        private Dictionary<int, LexerPath> _lexerPathMap; // TODO should perhaps be done as tree to make merging easier
        private GrammarContainer _grammarContainer;
        private LexerOptions _lexerOptions;
        private string _startGrammarName;
        private List<LexerPath> _invalidatedLexerPaths;

        public Lexer(GrammarContainer grammarContainer, string startGrammarName, LexerOptions lexerOptions)
        {
            _grammarContainer = grammarContainer;
            _startGrammarName = startGrammarName;
            _lexerOptions = lexerOptions;
            _sourceCodeLineList = new SourceCodeLineList();
            _invalidatedLexerPaths = new List<LexerPath>();
            Reset();
        }

        #region Properties

        public GrammarContainer GrammarContainer
        {
            get
            {
                return _grammarContainer;
            }
        }

        public SourceCodeLineList SourceCodeLines
        {
            get
            {
                return _sourceCodeLineList;
            }
            set
            {
                _sourceCodeLineList = value;
            }
        }

        public LexerOptions LexerOptions
        {
            get
            {
                return _lexerOptions;
            }
            set
            {
                _lexerOptions = value;
            }
        }

        protected int MaximumLexerPathId { get; set; }

        protected Dictionary<int, LexerPath> LexerPathMap
        {
            get
            {
                if (_lexerPathMap == null)
                {
                    _lexerPathMap = new Dictionary<int, LexerPath>();
                }
                return _lexerPathMap;
            }
        }

        protected List<LexerPath> InvalidatedLexerPaths
        {
            get
            {
                return _invalidatedLexerPaths;
            }
        }
        #endregion

        #region Public

        public void Reset()
        {
            LexerPathMap.Clear();
            MaximumLexerPathId = LexerPath.NOTSET;
            NewStartLexerPath();
        }

        public IEnumerable<List<Token>> NextTokens()
        {
            List<Token> tokenList = new List<Token>();
            List<LexerPath> currentLexerPaths;
            while (LexerPathMap.Count > 0)
            {
                tokenList.Clear();
                CheckForParserLexerPathInvalidation(tokenList);
                CheckForMergeLexerPaths(LexerPathMap.Values.ToList(), tokenList);
                currentLexerPaths = LexerPathMap.Values.ToList();
                foreach (LexerPath lexerPath in currentLexerPaths)
                {
                    NextTokens(lexerPath, tokenList);
                }
                    yield return tokenList;
            }
        }

        public void InvalidateLexerPath(int lexerPathID)
        {
            RemoveLexerPath(lexerPathID, true);
        }

        #endregion

        protected void CheckForParserLexerPathInvalidation(List<Token> tokenList)
        {
            if(LexerOptions.ReturnLexerPathTokens)
            {
                foreach(LexerPath invalidLexerPath in InvalidatedLexerPaths)
                {
                    tokenList.Add(new Token(invalidLexerPath.LexerPathID, Token.LEXERPATH_REMOVED, Token.LEXERPATH_EXTERN_REMOVED, invalidLexerPath.ActiveLineNumber, invalidLexerPath.ActiveCharacterNumber));
                }
            }
        }

        protected LexerPath ConfigureNewLexerPath(LexerPath lexerPath)
        {
            MaximumLexerPathId += 1;
            lexerPath.CurrentToken.LexerPathID = MaximumLexerPathId;
            lexerPath.LexerPathID = MaximumLexerPathId;
            LexerPathMap.Add(MaximumLexerPathId, lexerPath);
            return lexerPath;
        }

        protected void NewStartLexerPath()
        {
            LexerPath lexerPath = LexerPath.StartLexerPath();
            lexerPath.ActiveGrammar = GrammarContainer.GetGrammar(_startGrammarName);
            ConfigureNewLexerPath(lexerPath);
        }

        protected void CheckForMergeLexerPaths(List<LexerPath> currentLexerPaths, List<Token> tokenList)
        {
            foreach (LexerPath lexerPath in currentLexerPaths)
            {
                /* If the lexer path has split into a child lexer path the parent and child will 
                    have different path ids. It is expected that there are not a lot of paths as this 
                    might not be the most optimal solution */
                if (lexerPath.ParentLexerPathID != LexerPath.NOTSET && lexerPath.ParentLexerPathID != lexerPath.LexerPathID)
                {
                    LexerPath parentLexerPath;
                    if (LexerPathMap.TryGetValue(lexerPath.ParentLexerPathID, out parentLexerPath))
                    {
                        /* Same Grammar Name, same active line, same active character and last token was 
                            identified as the same (to avoid terminal shift due to grammar context) */
                        if (
                                (parentLexerPath.ActiveGrammar.GrammarName.Equals(lexerPath.ActiveGrammar.GrammarName))
                                && (parentLexerPath.ActiveLineNumber == lexerPath.ActiveLineNumber)
                                && (parentLexerPath.ActiveCharacterNumber == lexerPath.ActiveCharacterNumber)
                                && (parentLexerPath.CurrentToken.Terminal.Equals(lexerPath.CurrentToken.Terminal))
                            )
                        {
                            if (LexerOptions.ReturnLexerPathTokens)
                            {
                                tokenList.Add(new Token(lexerPath.LexerPathID, Token.LEXERPATH_MERGE, parentLexerPath.LexerPathID.ToString(), lexerPath.ActiveLineNumber, lexerPath.ActiveCharacterNumber));
                            }
                            RemoveLexerPath(lexerPath.LexerPathID);
                        }
                    }
                    else
                    {
                        throw new Exception("ParentLexerPathID does not exist which it should");
                    }
                }
            }
        }
        
        protected void RemoveLexerPath(int lexerPathID)
        {
            RemoveLexerPath(lexerPathID, false);
        }

        /* TODO make system able to handle multilevel parent child lexerPaths better */
        protected void RemoveLexerPath(int lexerPathID, bool invalidated)
        {
            /* Check if the lexerPath has already been removed */
            if (LexerPathMap.ContainsKey(lexerPathID))
            {
                LexerPath lexerPath;
                if (LexerPathMap.TryGetValue(lexerPathID, out lexerPath))
                {
                    if(invalidated)
                    {
                        InvalidatedLexerPaths.Add(lexerPath);
                    }
                    RemoveLexerPath(lexerPath);
                }
                else
                {
                    throw new Exception(string.Format("Could not get lexerPath {0}", lexerPathID));
                }
            }
        }
        protected void RemoveLexerPath(LexerPath lexerPath)
        {
            // Check for children and if existing promote children by replacing the ParentLexerPathID with the removed versions
            int parentLexerPathID = lexerPath.ParentLexerPathID;
            foreach (LexerPath childLexerPath in LexerPathMap.Values)
            {
                /* Check if child */
                if (childLexerPath.ParentLexerPathID == lexerPath.LexerPathID)
                {
                    childLexerPath.ParentLexerPathID = parentLexerPathID;
                }
            }
            LexerPathMap.Remove(lexerPath.LexerPathID);
        }

        protected void NextTokens(LexerPath lexerPath, List<Token> tokenList)
        {
            if (lexerPath.CurrentToken.Terminal.Equals(Token.NULL))
            {
                if (SourceCodeLines.Count != 0)
                {
                    lexerPath.ActiveLineNumber = Token.LINENUMBER_FIRSTLINE;
                }
                lexerPath.CurrentToken = new Token(lexerPath.LexerPathID, Token.BOF, string.Empty, lexerPath.ActiveLineNumber, lexerPath.ActiveCharacterNumber);
                tokenList.Add(lexerPath.CurrentToken);
            }
            // Check if EOF (sourceline empty)
            else if (SourceCodeLines.Count == 0 && lexerPath.CurrentToken.Terminal.Equals(Token.BOF))
            {
                lexerPath.CurrentToken = new Token(lexerPath.LexerPathID, Token.EOF, string.Empty, lexerPath.ActiveLineNumber, lexerPath.ActiveCharacterNumber);
                tokenList.Add(lexerPath.CurrentToken);
                if (LexerOptions.ReturnLexerPathTokens)
                {
                    tokenList.Add(new Token(lexerPath.LexerPathID, Token.LEXERPATH_REMOVED, Token.LEXERPATH_INTERN_REMOVED, lexerPath.ActiveLineNumber, lexerPath.ActiveCharacterNumber));
                }
                RemoveLexerPath(lexerPath.LexerPathID); // LexerPath should not be active any more
            }
            // Check if EOF (lastline EOL)
            else if (lexerPath.CurrentToken.Terminal.Equals(Token.EOL) && SourceCodeLines.Count <= lexerPath.ActiveLineNumber)
            {
                lexerPath.CurrentToken = new Token(lexerPath.LexerPathID, Token.EOF, string.Empty, lexerPath.ActiveLineNumber, lexerPath.ActiveCharacterNumber);
                tokenList.Add(lexerPath.CurrentToken);
                if (LexerOptions.ReturnLexerPathTokens)
                {
                    tokenList.Add(new Token(lexerPath.LexerPathID, Token.LEXERPATH_REMOVED, Token.LEXERPATH_INTERN_REMOVED, lexerPath.ActiveLineNumber, lexerPath.ActiveCharacterNumber));
                }
                RemoveLexerPath(lexerPath.LexerPathID); // LexerPath should not be active any more
            }
            else if (lexerPath.CurrentToken.Terminal.Equals(Token.BOF))
            {
                lexerPath.CurrentToken = new Token(lexerPath.LexerPathID, Token.BOL, string.Empty, lexerPath.ActiveLineNumber, lexerPath.ActiveCharacterNumber);
                lexerPath.ActiveCharacterNumber = Token.CHARPOSITION_START;
                tokenList.Add(lexerPath.CurrentToken);
            }
            // Check if BOL (lastline EOL)
            else if (lexerPath.CurrentToken.Terminal.Equals(Token.EOL) && SourceCodeLines.Count > lexerPath.ActiveLineNumber)
            {
                lexerPath.ActiveLineNumber += 1;
                lexerPath.ActiveCharacterNumber = Token.CHARPOSITION_START;
                lexerPath.CurrentToken = new Token(lexerPath.LexerPathID, Token.BOL, string.Empty, lexerPath.ActiveLineNumber, lexerPath.ActiveCharacterNumber);
                tokenList.Add(lexerPath.CurrentToken);
            }
            // check if EOL
            else if (SourceCodeLines.Line(lexerPath.ActiveLineNumber).Length <= lexerPath.ActiveCharacterNumber && !lexerPath.CurrentToken.Terminal.Equals(Token.EOL))
            {
                lexerPath.CurrentToken = new Token(lexerPath.LexerPathID, Token.EOL, string.Empty, lexerPath.ActiveLineNumber, lexerPath.ActiveCharacterNumber);
                tokenList.Add(lexerPath.CurrentToken);
            }
            // check indent level change after BOL
            else if (lexerPath.CurrentToken.Terminal.Equals(Token.BOL) && _lexerOptions.ReturnIndentTokens)
            {
                int indentCount = CountIndent(lexerPath, SourceCodeLines.Line(lexerPath.ActiveLineNumber));
                if (indentCount != lexerPath.ActiveIndentNumber)
                {

                    if (indentCount > lexerPath.ActiveIndentNumber)
                    {
                        lexerPath.CurrentToken = new Token(lexerPath.LexerPathID, Token.INDENT_INCREASED, indentCount.ToString(), lexerPath.ActiveLineNumber, lexerPath.ActiveCharacterNumber);
                        tokenList.Add(lexerPath.CurrentToken);
                    }
                    else
                    {
                        lexerPath.CurrentToken = new Token(lexerPath.LexerPathID, Token.INDENT_DECREASED, indentCount.ToString(), lexerPath.ActiveLineNumber, lexerPath.ActiveCharacterNumber);
                        tokenList.Add(lexerPath.CurrentToken);
                    }
                    lexerPath.ActiveIndentNumber = indentCount;
                }
                else
                {
                    lexerPath.CurrentToken = new Token(lexerPath.LexerPathID, Token.INDENT_UNCHANGED, indentCount.ToString(), lexerPath.ActiveLineNumber, lexerPath.ActiveCharacterNumber);
                    tokenList.Add(lexerPath.CurrentToken);
                }
            }
            else
            {
                List<TerminalMatch> terminalMatches = new List<TerminalMatch>();

                // Match only valid terminals from grammar
                foreach (Terminal terminal in lexerPath.Terminals)
                {
                    Match singleMatch = terminal.Match(SourceCodeLines.Line(lexerPath.ActiveLineNumber).Substring(lexerPath.ActiveCharacterNumber));
                    if (singleMatch.Success)
                    {
                        terminalMatches.Add(new TerminalMatch(terminal, singleMatch.Value));
                    }
                }

                if (terminalMatches.Count == 0)
                {
                    if (lexerPath.CurrentToken.Terminal.Equals(Token.EOF))
                    {
                        if (LexerOptions.ReturnLexerPathTokens)
                        {
                            tokenList.Add(new Token(lexerPath.LexerPathID, Token.LEXERPATH_REMOVED, Token.LEXERPATH_INTERN_REMOVED, lexerPath.ActiveLineNumber, lexerPath.ActiveCharacterNumber));
                        }
                        RemoveLexerPath(lexerPath.LexerPathID); // LexerPath should not be active any more
                    }
                    else
                    {
                        // if no match progress ActiveCharacterNumber 1 and return unknownterminal
                        lexerPath.CurrentToken = new Token(lexerPath.LexerPathID, Token.UNKNOWN_TERMINAL, SourceCodeLines.Line(lexerPath.ActiveLineNumber).Substring(lexerPath.ActiveCharacterNumber, 1), lexerPath.ActiveLineNumber, lexerPath.ActiveCharacterNumber + 1);
                        lexerPath.ActiveCharacterNumber += 1;
                        tokenList.Add(lexerPath.CurrentToken);
                    }
                }
                else
                {
                    List<LexerPath> lexerPathList = new List<LexerPath>();
                    
                    /* Split lexerPaths if needed */
                    for(int counter = 0; counter< terminalMatches.Count;counter++)
                    {
                        if(counter == 0)
                        {
                            lexerPathList.Add(lexerPath);
                        }
                        else
                        {
                            /* Add clones of LexerPath to LexerPathMap */
                            LexerPath newLexerPath = ConfigureNewLexerPath(lexerPath.Clone());
                            if (LexerOptions.ReturnLexerPathTokens)
                            {
                                tokenList.Add(new Token(newLexerPath.LexerPathID, Token.LEXERPATH_SPLIT, lexerPath.LexerPathID.ToString(), newLexerPath.ActiveLineNumber, newLexerPath.ActiveCharacterNumber));
                            }
                            lexerPathList.Add(newLexerPath);
                        }
                    }

                    int matchCounter = 0;

                    // if match is ok find terminal, progress _activeCharacterNumber and add token
                    foreach (TerminalMatch terminalMatch in terminalMatches)
                    {
                        LexerPath newLexerPath = lexerPathList[matchCounter];
                        if (!terminalMatch.IgnoreTerminal)
                        {
                            newLexerPath.CurrentToken = new Token(newLexerPath.LexerPathID, terminalMatch.Terminal.TerminalName, terminalMatch.Capture, newLexerPath.ActiveLineNumber, newLexerPath.ActiveCharacterNumber);
                        }
                        newLexerPath.ActiveCharacterNumber += terminalMatch.Capture.Length;
                        if (terminalMatch.IgnoreTerminal)
                        {
                            NextTokens(newLexerPath, tokenList);
                        }
                        else
                        {
                            tokenList.Add(newLexerPath.CurrentToken);
                        }
                        matchCounter += 1;
                    }
                }
            }
        }

        protected int CountIndent(LexerPath lexerPath, SourceCodeLine sourceCodeLine)
        {
            Match match = IndentRegex.Match(sourceCodeLine.ToString());
            if (match.Success)
            {
                Group group = match.Groups[1];
                string indentString = group.Value;
                int indentCount = indentString.Count(x => x == '\t') * _lexerOptions.IndentSpacePerTab;
                indentCount += indentString.Count(x => x == ' ');

                return indentCount;
            }
            else
            {
                return lexerPath.ActiveIndentNumber;
            }
        }
    }
}
