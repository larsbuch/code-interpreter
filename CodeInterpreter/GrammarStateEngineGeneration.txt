﻿- Read <<ProductionLine>>
	- Ignore comments
	- Ignore empty lines
	- Maintain physical line for debugging
- Split <<ProductionLine>> between <<Production>> and <<ProductionParts>>
- Add <<Production>> to <<ProductionPartList>> with <<Context>> with default <<Context>> as being the grammar name with link to <<StartProductionPart>>
- Parse <<ProductionParts>>
	- Add first <<ProductionToken>> into <ProductionPartList> with <<Production>> as <<ParentProductionPart>>  and <<StartProductionPart>> as <<PreviousProductionPart>>
	- Add second <<ProductionToken>> into <<ProductionPartList>> with <<Production>> as <<ParentProductionPart>> and first <<ProductionToken>> as <<PreviousProductionPart>>
		- If <<ProductionToken>> is a grouping start (paranthesis start) then add group as production and contents as new subproduction
		- If <<ProductionToken>> is a grouping end then return to processing <<ParentProductionPart>>
		- If <<ProductionToken>> is <<Or>> then restart <<Production>>/<<ProductionPart>> processing from last <<StartProductionPart>>
		- If <<ProductionToken>> is <<?>> then interpret as {0;1}
		- If <<ProductionToken>> is <<*>> then add unconstrained <<*>> to connection
		- If <<ProductionToken>> is {x;y} then repeat previous projection x times and add constrained <<*>> for (y-x) times
		- If <<ProductionToken>> is <<+>> then repeat previous production and add unconstrained <<*>> equivalent to {1;}
		- If <<ProductionToken>> is {x} interpret as {x;x}
		- If <<ProductionToken>> is {;x} interpret af {0;x}
		- If production contains <</>> only first match of lexerpath (at lexer spilt) is kept and all others are eliminated
- After parsion make literal grouping by replacing same terminals with generic terminal
- Generate "enumerations" of ProductionParts as grammar connections between grammar states
- Generate "enumerations" of Productions (non-terminals) as grammar states
- Generate "enumerations" of Contexts as ContextStates

Nice to have: Support for file format XMI 2.0 .uml

