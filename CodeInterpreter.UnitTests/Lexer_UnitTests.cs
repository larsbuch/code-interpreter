﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace CodeInterpreter.UnitTests
{
    public class Lexer_UnitTests
    {
        [Theory, CodeInterpreterTestConvensions]
        public void Lexer_CreateNewInstanceWithoutException(string testGrammarName)
        {
            Exception expected = null;
            Exception actual = null;
            Lexer lexer;
            try
            {
                lexer = new Lexer(GetTestGrammarContainer(testGrammarName, false), testGrammarName, new LexerOptions());
            }
            catch (Exception ex)
            {
                actual = ex;
            }
            Assert.Equal(expected, actual);
        }

        [Theory, CodeInterpreterTestConvensions]
        public void Lexer_SetEmptySourceCodeWithoutException(string testGrammarName)
        {
            Exception expected = null;
            Exception actual = null;
            Lexer lexer = new Lexer(GetTestGrammarContainer(testGrammarName, false), testGrammarName, new LexerOptions());
            try
            {
                lexer.SourceCodeLines = new SourceCodeLineList();
            }
            catch (Exception ex)
            {
                actual = ex;
            }
            Assert.Equal(expected, actual);
        }

        [Theory, CodeInterpreterTestConvensions]
        public void Lexer_ParseEmptySourceCodeWithoutException(string testGrammarName)
        {
            Exception expected = null;
            Exception actual = null;
            Lexer lexer = new Lexer(GetTestGrammarContainer(testGrammarName, false), testGrammarName, new LexerOptions());
            lexer.SourceCodeLines = new SourceCodeLineList();
            int tokens = 0;
            try
            {
                tokens = lexer.NextTokens().Cast<List<Token>>().Count();
            }
            catch (Exception ex)
            {
                actual = ex;
            }
            Assert.Equal(expected, actual);
            Assert.Equal(2, tokens);
        }

        [Theory, CodeInterpreterTestConvensions]
        public void Lexer_ParseEmptySourceCodeWithCorrectTokens(string testGrammarName)
        {
            Lexer lexer = new Lexer(GetTestGrammarContainer(testGrammarName, false), testGrammarName, new LexerOptions());
            lexer.SourceCodeLines = new SourceCodeLineList();
            List<string> tokenList = TokenizeAll(lexer).Select(s => s.Terminal).ToList();
            Assert.Equal(2, tokenList.Count);
            Assert.Equal(Token.BOF, tokenList[0]);
            Assert.Equal(Token.EOF, tokenList[1]);
        }

        [Theory, CodeInterpreterTestConvensions]
        public void Lexer_ParseEmptySourceCodeLineWithCorrectTokens(string testGrammarName)
        {
            Lexer lexer = new Lexer(GetTestGrammarContainer(testGrammarName, false), testGrammarName, new LexerOptions());
            SourceCodeLineList sourceCodeLineList = new SourceCodeLineList();
            sourceCodeLineList.Add(new SourceCodeLine(1, ""));
            lexer.SourceCodeLines = sourceCodeLineList;

            List<string> tokenList = TokenizeAll(lexer).Select(s => s.Terminal).ToList();
            Assert.Equal(4, tokenList.Count);
            Assert.Equal(Token.BOF, tokenList[0]);
            Assert.Equal(Token.BOL, tokenList[1]);
            Assert.Equal(Token.EOL, tokenList[2]);
            Assert.Equal(Token.EOF, tokenList[3]);
        }

        [Theory, CodeInterpreterTestConvensions]
        public void Lexer_NextTokens_TerminalsCorrect(string testGrammarName)
        {
            Lexer lexer = new Lexer(GetTestGrammarContainer(testGrammarName, true), testGrammarName, new LexerOptions());
            lexer.SourceCodeLines = GetSingleSourceCodeLine();

            List<string> tokenList = TokenizeAll(lexer).Select(s => s.Terminal).ToList();
            Assert.Equal(10, tokenList.Count);
            Assert.Equal(Token.BOF, tokenList[0]);
            Assert.Equal(Token.BOL, tokenList[1]);
            Assert.Equal("LARS", tokenList[2]);
            Assert.Equal("WORD", tokenList[3]);
            Assert.Equal("WORD", tokenList[4]);
            Assert.Equal("WORD", tokenList[5]);
            Assert.Equal("WORD", tokenList[6]);
            Assert.Equal("WORD", tokenList[7]);
            Assert.Equal(Token.EOL, tokenList[8]);
            Assert.Equal(Token.EOF, tokenList[9]);
        }

        [Theory, CodeInterpreterTestConvensions]
        public void Lexer_NextTokens_MatchesCorrect(string testGrammarName)
        {
            Lexer lexer = new Lexer(GetTestGrammarContainer(testGrammarName, true), testGrammarName, new LexerOptions());
            lexer.SourceCodeLines = GetSingleSourceCodeLine();

            List<string> tokenList = TokenizeAll(lexer).Select(s => s.Match).ToList();
            Assert.Equal(10, tokenList.Count);
            Assert.Equal(string.Empty, tokenList[0]);
            Assert.Equal(string.Empty, tokenList[1]);
            Assert.Equal("Lars", tokenList[2]);
            Assert.Equal("Lars", tokenList[3]);
            Assert.Equal("gik", tokenList[4]);
            Assert.Equal("gik", tokenList[5]);
            Assert.Equal("en", tokenList[6]);
            Assert.Equal("tur", tokenList[7]);
            Assert.Equal(string.Empty, tokenList[8]);
            Assert.Equal(string.Empty, tokenList[9]);
        }

        [Theory, CodeInterpreterTestConvensions]
        public void Lexer_NextTokens_LexerPathCorrect(string testGrammarName)
        {
            Lexer lexer = new Lexer(GetTestGrammarContainer(testGrammarName, true), testGrammarName, new LexerOptions());
            lexer.SourceCodeLines = GetSingleSourceCodeLine();

            List<int> tokenList = TokenizeAll(lexer).Select(s => s.LexerPathID).ToList();
            Assert.Equal(10, tokenList.Count);
            Assert.Equal(1, tokenList[0]);
            Assert.Equal(1, tokenList[1]);
            Assert.Equal(1, tokenList[2]);
            Assert.Equal(2, tokenList[3]);
            Assert.Equal(1, tokenList[4]);
            Assert.Equal(2, tokenList[5]);
            Assert.Equal(1, tokenList[6]);
            Assert.Equal(1, tokenList[7]);
            Assert.Equal(1, tokenList[8]);
            Assert.Equal(1, tokenList[9]);
        }

        [Theory, CodeInterpreterTestConvensions]
        public void Lexer_NextTokens_ActiveLineNumberAndCharNumberCorrect(string testGrammarName)
        {
            Lexer lexer = new Lexer(GetTestGrammarContainer(testGrammarName, true), testGrammarName, new LexerOptions());
            lexer.SourceCodeLines = GetSingleSourceCodeLine();

            List<Token> tokenList = TokenizeAll(lexer).ToList();
            Assert.Equal(10, tokenList.Count);
            Assert.Equal(Token.LINENUMBER_FIRSTLINE, tokenList[0].LinePosition);
            Assert.Equal(Token.CHARPOSITION_START, tokenList[0].CharPosition);
            Assert.Equal(Token.LINENUMBER_FIRSTLINE, tokenList[1].LinePosition);
            Assert.Equal(Token.CHARPOSITION_START, tokenList[1].CharPosition);
            Assert.Equal(1, tokenList[2].LinePosition);
            Assert.Equal(1, tokenList[2].CharPosition);
            Assert.Equal(1, tokenList[3].LinePosition);
            Assert.Equal(1, tokenList[3].CharPosition);
            Assert.Equal(1, tokenList[4].LinePosition);
            Assert.Equal(6, tokenList[4].CharPosition);
            Assert.Equal(1, tokenList[5].LinePosition);
            Assert.Equal(6, tokenList[5].CharPosition);
            Assert.Equal(1, tokenList[6].LinePosition);
            Assert.Equal(10, tokenList[6].CharPosition);
            Assert.Equal(1, tokenList[7].LinePosition);
            Assert.Equal(13, tokenList[7].CharPosition);
            Assert.Equal(1, tokenList[8].LinePosition);
            Assert.Equal(16, tokenList[8].CharPosition);
            Assert.Equal(1, tokenList[9].LinePosition);
            Assert.Equal(16, tokenList[9].CharPosition);
        }

        [Theory, CodeInterpreterTestConvensions]
        public void Lexer_LexerOptions_ReturnLexerPathTokens_TerminalsCorrect(string testGrammarName)
        {
            LexerOptions lexerOptions = new LexerOptions();
            lexerOptions.ReturnLexerPathTokens = true;

            Lexer lexer = new Lexer(GetTestGrammarContainer(testGrammarName, true), testGrammarName, lexerOptions);
            lexer.SourceCodeLines = GetSingleSourceCodeLine();

            List<string> tokenList = TokenizeAll(lexer).Select(s => s.Terminal).ToList();
            Assert.Equal(13, tokenList.Count);
            Assert.Equal(Token.BOF, tokenList[0]);
            Assert.Equal(Token.BOL, tokenList[1]);
            Assert.Equal(Token.LEXERPATH_SPLIT, tokenList[2]);
            Assert.Equal("LARS", tokenList[3]);
            Assert.Equal("WORD", tokenList[4]);
            Assert.Equal("WORD", tokenList[5]);
            Assert.Equal("WORD", tokenList[6]);
            Assert.Equal(Token.LEXERPATH_MERGE, tokenList[7]);
            Assert.Equal("WORD", tokenList[8]);
            Assert.Equal("WORD", tokenList[9]);
            Assert.Equal(Token.EOL, tokenList[10]);
            Assert.Equal(Token.EOF, tokenList[11]);
            Assert.Equal(Token.LEXERPATH_REMOVED, tokenList[12]);
        }

        [Theory, CodeInterpreterTestConvensions]
        public void Lexer_LexerOptions_ReturnIndentTokens_SimpleIndent(string testGrammarName)
        {
            LexerOptions lexerOptions = new LexerOptions();
            lexerOptions.ReturnIndentTokens = true;

            Lexer lexer = new Lexer(GetTestGrammarContainer(testGrammarName, true), testGrammarName, lexerOptions);
            lexer.SourceCodeLines = GetSingleSourceCodeLine();

            List<Token> tokenList = TokenizeAll(lexer).Where(s => s.Terminal.StartsWith("INDENT")).ToList();
            Assert.Equal(1, tokenList.Count);
            Assert.Equal(Token.INDENT_INCREASED, tokenList[0].Terminal);
            Assert.Equal(1.ToString(), tokenList[0].Match);
            Assert.Equal(Token.LINENUMBER_FIRSTLINE, tokenList[0].LinePosition);
            Assert.Equal(Token.CHARPOSITION_START, tokenList[0].CharPosition);
            Assert.Equal(1, tokenList[0].LexerPathID);
        }

        [Theory, CodeInterpreterTestConvensions]
        public void Lexer_LexerOptions_ReturnIndentTokens_AdvancedIndent(string testGrammarName)
        {
            LexerOptions lexerOptions = new LexerOptions();
            lexerOptions.ReturnIndentTokens = true;

            Lexer lexer = new Lexer(GetTestGrammarContainer(testGrammarName, true), testGrammarName, lexerOptions);
            lexer.SourceCodeLines = GetMultiSourceCodeLine();

            List<Token> tokenList = TokenizeAll(lexer).Where(s => s.Terminal.StartsWith("INDENT")).ToList();
            Assert.Equal(3, tokenList.Count);
            Assert.Equal(Token.INDENT_INCREASED, tokenList[0].Terminal);
            Assert.Equal(1.ToString(), tokenList[0].Match);
            Assert.Equal(Token.INDENT_INCREASED, tokenList[1].Terminal);
            Assert.Equal(3.ToString(), tokenList[1].Match);
            Assert.Equal(Token.INDENT_INCREASED, tokenList[2].Terminal);
            Assert.Equal(lexerOptions.IndentSpacePerTab.ToString(), tokenList[2].Match);
        }

        [Theory, CodeInterpreterTestConvensions]
        public void Lexer_LexerOptions_ReturnIndentTokens_AdvancedIndentChangeTab(string testGrammarName)
        {
            LexerOptions lexerOptions = new LexerOptions();
            lexerOptions.ReturnIndentTokens = true;
            lexerOptions.IndentSpacePerTab = 3;

            Lexer lexer = new Lexer(GetTestGrammarContainer(testGrammarName, true), testGrammarName, lexerOptions);
            lexer.SourceCodeLines = GetMultiSourceCodeLine();

            List<Token> tokenList = TokenizeAll(lexer).Where(s => s.Terminal.StartsWith("INDENT")).ToList();
            Assert.Equal(3, tokenList.Count);
            Assert.Equal(Token.INDENT_INCREASED, tokenList[0].Terminal);
            Assert.Equal(1.ToString(), tokenList[0].Match);
            Assert.Equal(Token.INDENT_INCREASED, tokenList[1].Terminal);
            Assert.Equal(3.ToString(), tokenList[1].Match);
            Assert.Equal(Token.INDENT_UNCHANGED, tokenList[2].Terminal);
            Assert.Equal(lexerOptions.IndentSpacePerTab.ToString(), tokenList[2].Match);
        }

        [Theory, CodeInterpreterTestConvensions]
        public void Lexer_LexerOptions_ReturnIndentTokens_AdvancedIndentChangeTab2(string testGrammarName)
        {
            LexerOptions lexerOptions = new LexerOptions();
            lexerOptions.ReturnIndentTokens = true;
            lexerOptions.IndentSpacePerTab = 1;

            Lexer lexer = new Lexer(GetTestGrammarContainer(testGrammarName, true), testGrammarName, lexerOptions);
            lexer.SourceCodeLines = GetMultiSourceCodeLine();

            List<Token> tokenList = TokenizeAll(lexer).Where(s => s.Terminal.StartsWith("INDENT")).ToList();
            Assert.Equal(3, tokenList.Count);
            Assert.Equal(Token.INDENT_INCREASED, tokenList[0].Terminal);
            Assert.Equal(1.ToString(), tokenList[0].Match);
            Assert.Equal(Token.INDENT_INCREASED, tokenList[1].Terminal);
            Assert.Equal(3.ToString(), tokenList[1].Match);
            Assert.Equal(Token.INDENT_DECREASED, tokenList[2].Terminal);
            Assert.Equal(lexerOptions.IndentSpacePerTab.ToString(), tokenList[2].Match);
        }

        [Theory, CodeInterpreterTestConvensions]
        public void Lexer_NextTokens_ReturnLexerPathTokens_MultiSourceCodeLines(string testGrammarName)
        {
            LexerOptions lexerOptions = new LexerOptions();
            lexerOptions.ReturnLexerPathTokens = true;

            Lexer lexer = new Lexer(GetTestGrammarContainer(testGrammarName, true), testGrammarName, lexerOptions);
            lexer.SourceCodeLines = GetMultiSourceCodeLine();

            List<Token> tokenList = TokenizeAll(lexer).Where(s => s.Terminal.StartsWith("LEXERPATH")).ToList();
            Assert.Equal(5, tokenList.Count);
            Assert.Equal(Token.LEXERPATH_SPLIT, tokenList[0].Terminal);
            Assert.Equal(2, tokenList[0].LexerPathID);
            Assert.Equal(1.ToString(), tokenList[0].Match);
            Assert.Equal(Token.LEXERPATH_MERGE, tokenList[1].Terminal);
            Assert.Equal(2, tokenList[1].LexerPathID);
            Assert.Equal(1.ToString(), tokenList[1].Match);
            Assert.Equal(Token.LEXERPATH_SPLIT, tokenList[2].Terminal);
            Assert.Equal(3, tokenList[2].LexerPathID);
            Assert.Equal(1.ToString(), tokenList[2].Match);
            Assert.Equal(Token.LEXERPATH_MERGE, tokenList[3].Terminal);
            Assert.Equal(3, tokenList[3].LexerPathID);
            Assert.Equal(1.ToString(), tokenList[3].Match);
            Assert.Equal(Token.LEXERPATH_REMOVED, tokenList[4].Terminal);
            Assert.Equal(1, tokenList[4].LexerPathID);
            Assert.Equal(Token.LEXERPATH_INTERN_REMOVED, tokenList[4].Match);
        }

        [Theory, CodeInterpreterTestConvensions]
        public void Lexer_ParserInvalidateLexerPath_CorrectRemove(string testGrammarName)
        {
            LexerOptions lexerOptions = new LexerOptions();
            lexerOptions.ReturnLexerPathTokens = true;

            Lexer lexer = new Lexer(GetTestGrammarContainer(testGrammarName, true), testGrammarName, lexerOptions);
            lexer.SourceCodeLines = GetMultiSourceCodeLine();

            List<Token> tokenList = TokenizeSome(lexer,3).ToList();
            Assert.Equal(5, tokenList.Count);
            Assert.Equal(Token.BOF, tokenList[0].Terminal);
            Assert.Equal(1, tokenList[0].LexerPathID);
            Assert.Equal(Token.BOL, tokenList[1].Terminal);
            Assert.Equal(1, tokenList[1].LexerPathID);
            Assert.Equal(Token.LEXERPATH_SPLIT, tokenList[2].Terminal);
            Assert.Equal(2, tokenList[2].LexerPathID);
            Assert.Equal(1.ToString(), tokenList[2].Match);
            Assert.Equal("LARS", tokenList[3].Terminal);
            Assert.Equal(1, tokenList[3].LexerPathID);
            Assert.Equal("Lars", tokenList[3].Match);
            Assert.Equal("WORD", tokenList[4].Terminal);
            Assert.Equal(2, tokenList[4].LexerPathID);
            Assert.Equal("Lars", tokenList[4].Match);
            lexer.InvalidateLexerPath(2);
            tokenList = TokenizeSome(lexer, 1).ToList();
            Assert.Equal(2, tokenList.Count);
            Assert.Equal(Token.LEXERPATH_REMOVED, tokenList[0].Terminal);
            Assert.Equal(2, tokenList[0].LexerPathID);
            Assert.Equal(Token.LEXERPATH_EXTERN_REMOVED, tokenList[0].Match);
            Assert.Equal("WORD", tokenList[1].Terminal);
            Assert.Equal(1, tokenList[1].LexerPathID);
            Assert.Equal("og", tokenList[1].Match);
        }

        #region Helper Methods

        private GrammarContainer GetTestGrammarContainer(string grammarName, bool ignoreSpace)
        {
            Grammar grammar = new Grammar(grammarName);
            grammar.AddTerminal(new Terminal("SPACE", " ", ignoreSpace));
            grammar.AddTerminal(new Terminal("LARS", "Lars", false));
            grammar.AddTerminal(new Terminal("RITA", "Rita", false));
            grammar.AddTerminal(new Terminal("WORD", "\\w+", false));

            GrammarContainer grammarContainer = new GrammarContainer();
            grammarContainer.AddGrammar(grammar);

            return grammarContainer;
        }

        private SourceCodeLineList GetSingleSourceCodeLine()
        {
            SourceCodeLine singleTextLine = new SourceCodeLine(1, " Lars gik en tur");
 
            SourceCodeLineList sourceCodeLineList = new SourceCodeLineList();
            sourceCodeLineList.Add(singleTextLine);
            return sourceCodeLineList;
        }

        private SourceCodeLineList GetMultiSourceCodeLine()
        {
            SourceCodeLineList sourceCodeLineList = new SourceCodeLineList();
            sourceCodeLineList.Add(new SourceCodeLine(1, " Lars og Rita gik en tur."));
            sourceCodeLineList.Add(new SourceCodeLine(2, "   De så en isbjørn"));
            sourceCodeLineList.Add(new SourceCodeLine(3, "\tSenere kørte de hjem"));
            return sourceCodeLineList;
        }

        private IEnumerable<Token> TokenizeAll(Lexer lexer)
        {
            foreach (List<Token> nextTokens in lexer.NextTokens())
            {
                foreach (Token token in nextTokens)
                {
                    yield return token;
                }
            }
        }

        private IEnumerable<Token> TokenizeSome(Lexer lexer, int someSpecified)
        {
            int counter = 0;
            foreach (List<Token> nextTokens in lexer.NextTokens())
            {
                foreach (Token token in nextTokens)
                {
                    yield return token;
                }
                counter += 1;
                if(counter == someSpecified)
                {
                    yield break;
                }
            }
        }

        #endregion
    }
}
